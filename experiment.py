import dimacs
import dpll
import numpy as np
import matplotlib.pyplot as plt
import glob
from itertools import product
from scipy.stats import ttest_ind
import os

def get_amount_files(folder_path):
    list = glob.glob(folder_path + '*.dimacs')
    return len(list)

def get_sudoku_cnf(name_dataset, index, dim_rules):
    sudoku = dimacs.parse_dimacs(dimacs.get_dimacs_path(name_dataset) + str(index) + ".dimacs")
    rules = dimacs.parse_dimacs(dim_rules)
    return (sudoku + rules)

def print_debug(sat, assignments, backtrack_count, recursion_count, unit_clause_count):
    print("Is it satisfiable?: " + str(sat))
    print("Amount assignments: " + str(len(assignments)) + "\nAssignments: ")
    print(assignments)
    print("Backtrack count: " + str(backtrack_count))
    print("Recursion count: " + str(recursion_count))
    print("Unit clause solved count: " + str(unit_clause_count))

def run_experiment(list_strategies, name_dataset, dim_rules):
    bt_count_data = ["Backtrack boxplot", []]
    rc_count_data = ["Recursion boxplot", []]
    uc_count_data = ["Unit clause solving boxplot", []]

    amount_of_files = get_amount_files(dimacs.get_dimacs_path(name_dataset))
    chosen_strategy = 0
    for strategy, sudoku_index in product(list_strategies, range(amount_of_files)):
        print(strategy, sudoku_index)
        if chosen_strategy != strategy:
            bt_count = []
            rc_count = []
            uc_count = []
            chosen_strategy = strategy

        cnf = get_sudoku_cnf(name_dataset, sudoku_index, dim_rules)
        sat, assignments, backtrack_count, recursion_count, unit_clause_count = dpll.dpll(cnf, strategy=strategy)
        # Append results
        if sat:
            bt_count.append(backtrack_count)
            rc_count.append(recursion_count)
            uc_count.append((unit_clause_count / recursion_count))
            assignments.sort(reverse=True)
            # Solution is exported to ./dimacs/name_dataset/solutions-s1/ (where 1 is the strategy indication)
            dimacs.solution_to_dimac(name_dataset, sudoku_index, strategy, assignments)
        if sudoku_index == (amount_of_files - 1):
            bt_count_data[1].append(bt_count)
            rc_count_data[1].append(rc_count)
            uc_count_data[1].append(uc_count)
    if len(list_strategies) == 2:
        test_for_significance(bt_count_data[1][1], bt_count_data[1][0], 'less', name_dataset, "significance_test_bt")
        test_for_significance(rc_count_data[1][1], rc_count_data[1][0], 'less', name_dataset, "significance_test_rc")
        test_for_significance(uc_count_data[1][1], uc_count_data[1][0], 'greater', name_dataset, "significance_test_uc")
    generate_boxplot(bt_count_data, name_dataset)
    generate_boxplot(rc_count_data, name_dataset)
    generate_boxplot(uc_count_data, name_dataset)


def generate_boxplot(data, dataset_name):
    # Fixing random state for reproducibility
    fig1, ax1 = plt.subplots()
    ax1.set_title(data[0])
    ax1.boxplot(data[1], 0, '', showmeans=True)
    folder_name = './results/' + dataset_name.split('/')[-1] + '/'
    os.makedirs(os.path.dirname(folder_name), exist_ok=True)
    plt.savefig(folder_name + data[0] + '.png')
    plt.show()

# T Test
def test_for_significance(dataset_1, dataset_2, alternative_hypothesis, dataset_name, test_name):
    ttest, pval = ttest_ind(dataset_1, dataset_2, alternative=alternative_hypothesis)

    print(type(ttest), type(pval), ttest, pval)

    print("t-test", '{0:.10f}'.format(ttest))
    print("p-value", '{0:.10f}'.format(pval))

    sign_result = ""
    if pval <0.05:
        sign_result = "we reject null hypothesis"
    else:
        sign_result = "we accept null hypothesis"

    # Output to a text file inside results folder
    filename = './results/' + dataset_name.split('/')[-1] + '/' + test_name + '.txt'
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    with open(filename, "w") as f:
        f.write("t-test {} \n".format(ttest))
        f.write("p-value {} \n".format(pval))
        f.write("{} \n".format(sign_result))

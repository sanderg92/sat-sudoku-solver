#!/usr/bin/env python
# -*- coding: utf-8 -*-

# IMPORTS
import argparse
import dimacs
import experiment

# FILE CONSTS
SUD4X4 = "test_sudokus/4x4.txt"
SUD16X16 = "test_sudokus/16x16.txt"
SUD1000 = "test_sudokus/1000 sudokus.txt"
DAMNHARD = "test_sudokus/damnhard.sdk.txt"
SUBIG20 = "test_sudokus/subig20.sdk.txt"
TOP91 = "test_sudokus/top91.sdk.txt"
TOP95 = "test_sudokus/top95.sdk.txt"
TOP100 = "test_sudokus/top100.sdk.txt"
TOP870 = "test_sudokus/top870.sdk.txt"
TOP2365 = "test_sudokus/top2365.sdk.txt"
# RULES
RLS4X4 = "rules/sudoku-rules-4x4.dimacs"
RLS9X9 = "rules/sudoku-rules-9x9.dimacs"
RLS16X16 = "rules/sudoku-rules-16x16.dimacs"

def main():
    """ Main program """
    parser = argparse.ArgumentParser()
    subparser = parser.add_subparsers(dest='command')

    create_datasets = subparser.add_parser('create_datasets')

    run_experiment = subparser.add_parser('run_experiment')
    run_experiment.add_argument('-s','--list', nargs='+', type=int, help='<Required> Set flag', required=True)
    run_experiment.add_argument('--dataset', type=int, required=True)

    args = parser.parse_args()

    if args.command == 'create_datasets':
        list_datasets = [SUD4X4, SUD16X16, SUD1000, DAMNHARD, SUBIG20, TOP91, TOP95, TOP100, TOP870, TOP2365]
        dimacs.create_datasets(list_datasets)

    if args.command == 'run_experiment':
        experiment_configs = [[SUD4X4, RLS4X4],
                            [SUD16X16, RLS16X16],
                            [SUD1000, RLS9X9],
                            [DAMNHARD, RLS9X9],
                            [SUBIG20, RLS9X9],
                            [TOP91, RLS9X9],
                            [TOP95, RLS9X9],
                            [TOP100, RLS9X9],
                            [TOP870, RLS9X9],
                            [TOP2365, RLS9X9],
        ]
        experiment.run_experiment(args.list, experiment_configs[args.dataset][0], experiment_configs[args.dataset][1])

if __name__ == "__main__":
    main()
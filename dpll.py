from itertools import product, chain
from collections import defaultdict, Counter
import math
import random
import copy

def random_selection(cnf):
    return random.choice(list(chain.from_iterable(cnf)))

def random_lit_from_shortest_clause(cnf):
    shortest_clause = min(cnf, key=len)
    return random.choice(shortest_clause)

def dlcs(cnf):
    dict_cnf = Counter(chain(*cnf))
    largest_combined_sum = 0 
    chosen_lit = ""
    for l in list(dict_cnf.keys()):
        l = abs(l)
        sum = dict_cnf[l] + dict_cnf[-l]
        if sum > largest_combined_sum:
            largest_combined_sum = sum 
            if dict_cnf[l] > dict_cnf[-l]:
                chosen_lit = l
            else:
                chosen_lit = -l
    return chosen_lit

def dlis(cnf):
    dict_cnf = Counter(chain(*cnf))
    l = abs(dict_cnf.most_common(1)[0][0])
    chosen_lit = ""
    if dict_cnf[l] > dict_cnf[-l]:
        chosen_lit = l
    else:
        chosen_lit = -l
    return chosen_lit

def mom_selection(cnf, k = 2):
    minimal_len_clauses = [clause for clause in cnf if len(clause) == 2]
    dict_min_clauses = Counter(chain(*minimal_len_clauses))
    chosen_lit = 0
    max_result = 0
    for literal in list(dict_min_clauses.keys()):
        l = abs(literal)
        moms_result = ((dict_min_clauses[l] + dict_min_clauses[-l]) * 2**k) + (dict_min_clauses[l] * dict_min_clauses[-l])
        if moms_result > max_result:
            chosen_lit = l
            max_result = moms_result
    return chosen_lit

def combination_selection(cnf):
    shortest_clause = min(cnf, key=len)
    occurances = Counter(chain(*cnf))
    max_occurances = 0 
    result = 0
    for l in shortest_clause:
        if occurances[l] > max_occurances:
            max_occurances = occurances[l]
            result = l
    return result

def jw_os(cnf):
    points = defaultdict(int) 
    dict_cnf = Counter(chain(*cnf))
    for literal, clause in product(list(dict_cnf.keys()), cnf):
        if literal in clause:
            increment = math.pow(2, -len(clause))
            points[literal] += increment
    return max(points, key=points.get)

def jw_ts(cnf):
    points = defaultdict(int) 
    dict_cnf = Counter(chain(*cnf))
    for literal, clause in product(list(dict_cnf.keys()), cnf):
        if literal in clause:
            increment = math.pow(2, -len(clause)) #The Jeroslow-Wang function.
            points[literal] += increment
    chosen_literal = 0
    max_j = 0
    for l in list(dict_cnf.keys()):
        abs_l = abs(l)
        sum_j = points[abs_l] + points[-abs_l]
        if sum_j > max_j:
            max_j = sum_j
            if points[abs_l] > points[-abs_l]:
                chosen_literal = abs_l
            else:
                chosen_literal = -abs_l
    return chosen_literal

def select_literal(cnf, strategy = 1):
    selected_literal = 0
    if strategy == 1:
        selected_literal = random_selection(cnf)
    elif strategy == 2:
        selected_literal = random_lit_from_shortest_clause(cnf)
    elif strategy == 3:
        selected_literal = dlcs(cnf)
    elif strategy == 4:
        selected_literal = dlis(cnf)
    elif strategy == 5:
        selected_literal = mom_selection(cnf)
    elif strategy == 6:
        selected_literal = combination_selection(cnf)
    elif strategy == 7:
        selected_literal = jw_os(cnf)
    elif strategy == 8:
        selected_literal = jw_ts(cnf)
    return selected_literal

def simplify(cnf, l):
    new_cnf = [c for c in cnf if l not in c]
    new_cnf = [[ele for ele in c if ele != -l] for c in new_cnf]
    return new_cnf

def dpll(cnf, assignments=[], backtrack_count = 0, recursion_count = 0, unit_clause_count = 0, strategy = 1):
    if check_done(cnf) != None: return check_done(cnf), assignments, backtrack_count, recursion_count, unit_clause_count

    # print("Current length of assignments:")
    # print(len(assignments))

    copy_bc_count = copy.deepcopy(backtrack_count)
    copy_rc_count = copy.deepcopy(recursion_count)
    copy_uc_count = copy.deepcopy(unit_clause_count)

    # Unit rule
    new_cnf, new_assignments, uc_count = remove_unit_clauses(cnf, assignments)
    # Check if unit rule happened, if so start recursion
    if len(new_cnf) < len(cnf):
        copy_uc_count += uc_count
        copy_rc_count += 1
        sat, vals, copy_bc_count, copy_rc_count, copy_uc_count = dpll(new_cnf, new_assignments, copy_bc_count, copy_rc_count, copy_uc_count, strategy=strategy)
        return sat, vals, copy_bc_count, copy_rc_count, copy_uc_count

    # If no unit rule happened, split happens according to selected heuristic
    l = select_literal(new_cnf, strategy)
    # Try l=T
    new_cnf_t = simplify(new_cnf, l)
    new_assignments.append(l)
    copy_rc_count += 1
    sat, vals, copy_bc_count, copy_rc_count, copy_uc_count = dpll(new_cnf_t, new_assignments, copy_bc_count, copy_rc_count, copy_uc_count, strategy=strategy)
    if sat:
        return sat, vals, copy_bc_count, copy_rc_count, copy_uc_count
    else: 
        new_assignments.remove(l)
    # Backtracking
    copy_bc_count += 1
    # Try l=F
    new_cnf_f = simplify(new_cnf, -l)
    new_assignments.append(-l)
    copy_rc_count += 1
    sat, vals, copy_bc_count, copy_rc_count, copy_uc_count = dpll(new_cnf_f, new_assignments, copy_bc_count, copy_rc_count, copy_uc_count, strategy=strategy)
    if sat:
        return sat, vals, copy_bc_count, copy_rc_count, copy_uc_count

    return False, None, copy_bc_count, copy_rc_count, copy_uc_count

def remove_unit_clauses(cnf, assignments):
    new_cnf = copy.deepcopy(cnf)
    copy_assignm = copy.deepcopy(assignments)
    for clause in cnf:
        if ((len(clause) == 1) and (clause[0] > 0)):
            if clause[0] not in copy_assignm:
                copy_assignm.append(clause[0])
            new_cnf = simplify(new_cnf, clause[0])
    for clause in new_cnf:
        if ((len(clause) == 1) and (clause[0] < 0)):
            if clause[0] not in copy_assignm:
                copy_assignm.append(clause[0])
            new_cnf = simplify(new_cnf, clause[0])
    unit_clause_count = len(cnf) - len(new_cnf)
    return new_cnf, copy_assignm, unit_clause_count

def check_done(cnf):
    if len(cnf) == 0:
        return True
    if any([len(c)==0 for c in cnf]):
        return False
    return None
import os
import numpy as np
import math

def get_dimacs_path(name_dataset):
    return "./dimacs/" + name_dataset.split("/")[-1] + "/"

def parse_dimacs(filepath):
    f = open(filepath, 'r')
    in_data = f.read().splitlines()
    cnf = list()
    cnf.append(list())
    # Following code based on snippet of CliffordVienna from StackExchange
    # https://stackoverflow.com/a/30181011
    for line in in_data:
        tokens = line.split()
        if len(tokens) != 0 and tokens[0] not in ("p", "c"):
            for tok in tokens:
                lit = int(tok)
                if lit == 0:
                    cnf.append(list())
                else:
                    cnf[-1].append(lit)
    assert len(cnf[-1]) == 0
    cnf.pop()
    return cnf

def solution_to_dimac(name_dataset, index, strategy, data):
    dimacs_filename = get_dimacs_path(name_dataset) + "solutions-s" + str(strategy) + "/" + str(index) + ".dimacs" 
    if not os.path.exists(dimacs_filename):
        os.makedirs(os.path.dirname(dimacs_filename), exist_ok=True)
        with open(dimacs_filename, "w") as f:
            # write the header
            f.write("p cnf {} {}\n".format(len(data), len(data)))
            # Write the clauses
            for clause in data:
                f.write("{} 0\n".format(clause))
        print(dimacs_filename + " file is created!")
    else:
        print("It already exists!")


def write_dimacs(name_dataset, sudoku_data):
    for index, data in enumerate(sudoku_data):
        dimacs_filename = get_dimacs_path(name_dataset) + str(index) + ".dimacs" 
        if not os.path.exists(dimacs_filename):
            os.makedirs(os.path.dirname(dimacs_filename), exist_ok=True)
            with open(dimacs_filename, "w") as f:
                # write the header
                f.write("p cnf {} {}\n".format(len(data), len(data)))
                # Write the clauses
                for clause in data:
                    f.write("{}\n".format(clause))
            print(dimacs_filename + " file is created!")
        else:
            print("It already exists!")


def encode_sudokus_to_dimacs(txt_dataset):
    raw_data = np.genfromtxt(txt_dataset, delimiter=1) 
    sudoku_dimensions = (int(math.sqrt(len(raw_data[0]))),int(math.sqrt(len(raw_data[0]))))
    ndim_data =  [np.reshape(arr, sudoku_dimensions) for arr in raw_data]
    sudokus = []
    for data in ndim_data:
        sudoku = []
        for idx, val in enumerate(data):
            for idy, elem in enumerate(val):
                if math.isnan(elem):
                    pass
                else: 
                    clause = str(idx +1) + str(idy +1) + str(int(elem)) + " 0"
                    sudoku.append(clause)
        sudokus.append(sudoku)
    write_dimacs(txt_dataset, sudokus)

def create_datasets(list_datasets):
    for dataset in list_datasets:
        encode_sudokus_to_dimacs(dataset)
    print("Datasets are created!")

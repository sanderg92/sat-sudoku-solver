# Sudoku SAT Solver 

A SAT solver designed to solve Sudoku problems. Developed by Sander Goetzee and Konstantin Mihhailov, group 55 of Knowledge Representation at Vrije Universiteit Amsterdam. 

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the following packages:

```bash
pip install argparse numpy itertools matplotlib scipy
```

## Usage

```python
# To create the DIMACS datasets from the .txt files (NECESSARY)
python3 main.py create_datasets
# To run experiment
    # Example command to run experiment with strategies 5,6 and dataset Sudoku 1000
python3 main.py run_experiment -s 5 6 --dataset 2
# Strategy flag -s n 
    # n can be multiple integers seperated by space 
    # NOTE: if there are more than 2 strategies selected, no T-test will happen
        # Strategies:
            # 1 = Random literal selection (Basic DPLL)
            # 2 = Random literal from shortest clause
            # 3 = DLCS
            # 4 = DLIS
            # 5 = Mom's selection (tuning parameter k=2, if other is needed change in code)
            # 6 = Mixed heuristic 
            # 7 = JW_OS
            # 8 = JW_TS
# Dataset name flag --dataset n
    # n should be an integer between 0 and 9
        # Datasets:
            # 0 = Sudoku 4x4 dataset
            # 1 = Sudoku 16x16 dataset (Haven't tested this dataset, focus was 9x9)
            # 2 = Sudoku 1000 9x9 dataset 
            # 3 = Sudoku Damnhard 9x9 dataset 
            # 4 = Sudoku SUBIG20 9x9 dataset 
            # 5 = Sudoku TOP91 9x9 dataset 
            # 6 = Sudoku TOP95 9x9 dataset  
            # 7 = Sudoku TOP100 9x9 dataset 
            # 8 = Sudoku TOP870 9x9 dataset  
            # 9 = Sudoku TOP2365 9x9 dataset 
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)